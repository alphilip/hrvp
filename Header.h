#pragma once

#ifndef HEADER_HPP
#define HEADER_HPP

#include <iostream>

const int nbmax_clients=500;
const int nbmax_clients_par_tournee = 20; //faire une exception lorsqu'on d�passe cette valeur : sdd sous dimensionee
const int nb_camion = 50;

typedef struct T_CAMION
{
	int nc; //nom du camion
	int capacite;
	int cout_variable; //cout par kilometre
	int cout_fixe;
}T_CAMION;

typedef struct T_INSTANCE {
	int d[nbmax_clients][nbmax_clients];//tableau des distances dij : attention, grands nombres
	int nc;//nombre de clients
	int nt; //nombre de type de v�hicules
	int Q[2000];//quantit�s 
	int nb_t[2000];//nombre de type de v�hicules existant
	T_CAMION camion[nb_camion];
}T_INSTANCE;

typedef struct T_TOURNEE
{
	int n;  //taille de la tournee
	int liste[nbmax_clients_par_tournee];  //ordre des points de la tournee
	int nc;  // nombre de clients
	int val; //val=somme des quantites des points de la tournee
	int cout; //cout=somme des distances entre 2 points * leur cout variable + cout fixe
}T_TOURNEE;



typedef struct T_SOLUTION
{
	int nt; //nombre de tournees
	T_TOURNEE liste[2000]; //ensemble des tournees
	int cout; //cout de l'ensemble des tournees
}T_SOLUTION;


void plus_proches_voisins(T_INSTANCE tournee_initiale);
void plus_proches_voisins_randomise(T_INSTANCE tournee_initiale);
//heuristique de notre choix
void recherche_locale();
void mvt_2_OPT_tournee();
void mvt_2_OPT_inter_tournee();
void deplacement_sommet();
void swap();
void split();
void GRASP();
#endif